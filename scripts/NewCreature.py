import sys
import xml.etree.ElementTree as etree
import pyqrcode
import png
import urllib3
import csv
from PIL import Image

#Statics for cards
bgColor = {'Tundra':'333333',
           'Desert':'b6571d',
           'Marine':'2e37fe',
           'Forest':'377105',
           'Jungle':'29ab87',
           'Grasslands':'e4ca7b'}

summonImg = {'t':'tundra.png',
             'f':'forest.png',
             'd':'desert.png',
             'm':'marine.png',
             'j':'jungle.png',
             'g':'grasslands.png',
             '1':'1.png',
             '2':'2.png',
             '3':'3.png',
             '4':'4.png',
             '5':'5.png',
             '6':'6.png'}

#Big Icon Definitions (trust me)
icons ={}
icons['Tundra']='M 59.49108,8.9412285 59.198531,8.7683581 58.400667,9.1938845 V 8.6220828 L 58.187903,8.5024031 57.988438,8.6220828 v 0.8909477 l -0.545208,0.279253 v -0.611695 l -0.545207,0.305847 V 8.9146325 L 57.722482,8.5024031 V 8.2231511 L 57.429933,8.0635778 56.898023,8.3428309 V 7.4784781 l -0.265954,-0.186168 -0.265954,0.186168 V 8.3428309 L 55.900694,8.0635778 55.674632,8.2098533 V 8.515701 l 0.691483,0.3989315 v 0.571803 l -0.545207,-0.292551 v 0.598399 L 55.315594,9.4997325 55.328884,8.6619764 55.062929,8.5024031 54.863463,8.6619764 V 9.2071825 L 54.092195,8.7816551 53.77305,8.9279305 v 0.345742 l 0.757971,0.412229 -0.492017,0.279252 v 0.3058485 l 0.279253,0.132978 0.704779,-0.4122305 0.492016,0.2792525 -0.492016,0.305848 0.505314,0.292549 h 0.01329 l -0.492016,0.29255 -0.718077,-0.425527 -0.29255,0.159573 v 0.319146 l 0.505313,0.239358 -0.771268,0.438825 v 0.345742 l 0.332444,0.132976 0.75797,-0.425526 v 0.545206 l 0.186168,0.146276 0.226061,-0.159574 v -0.824459 l 0.545207,-0.279252 v 0.598398 L 56.3661,11.640667 v 0.571803 l -0.691481,0.398932 v 0.305847 l 0.226062,0.146276 0.465421,-0.279253 v 0.864353 l 0.265954,0.186168 0.265954,-0.186168 v -0.864353 l 0.465421,0.279253 0.226061,-0.11968 V 12.611402 L 56.89801,12.21247 v -0.571803 l 0.545207,0.305849 v -0.558505 l 0.545207,0.29255 v 0.824459 l 0.212764,0.146275 0.199466,-0.146275 v -0.545207 l 0.744672,0.412229 0.345741,-0.146274 v -0.332444 l -0.718077,-0.438825 0.452123,-0.252656 v -0.29255 L 58.972456,10.74972 58.24108,11.148651 57.722469,10.882697 58.227783,10.590148 57.749065,10.2843 58.24108,9.9651535 58.972456,10.390682 59.225113,10.257704 V 9.9518555 l -0.478719,-0.265954 0.744673,-0.425527 z m -2.593057,2.0345525 h -0.478718 l -0.252656,-0.398931 0.239359,-0.425528 h 0.478718 l 0.226061,0.425528 z'
icons['Forest']='m 56.554294,7.470604 -1.256188,1.573098 0.462835,-5.53e-4 -0.727465,0.911568 0.523322,-5.53e-4 -0.856542,1.073035 0.584348,-5.56e-4 -1.077967,1.350068 1.977714,-0.0017 v 0.904547 c 0,0.208894 0.168053,0.376938 0.376964,0.376938 h 0.311076 c 0.208908,0 0.376964,-0.168044 0.376964,-0.376938 v -0.905087 l 1.808132,-0.0017 -1.089847,-1.347908 0.446092,-5.56e-4 -0.866264,-1.0714145 0.531964,-5.53e-4 -0.736107,-0.909944 0.481737,-5.53e-4 z'
icons['Jungle']='m 56.691771,12.661475 c -0.01873,-0.596574 -0.16576,-2.373418 -1.238824,-3.2130958 -0.202298,0.295072 -0.650758,1.1047828 -0.544411,2.3965518 -0.146343,-0.570034 -0.495757,-1.473814 -0.250185,-2.6472378 -0.373656,0.08381 -1.045572,0.268141 -1.199796,0.537408 0.0727,-0.364854 0.645564,-1.156361 1.25748,-1.316793 -0.0016,-0.0012 -0.003,-0.0023 -0.0044,-0.003 0.0014,-0.0015 0.0029,-0.0023 0.0044,-0.0037 -0.116338,-0.250692 -0.387885,-0.762323 -0.690564,-0.82565 0.293448,-0.01476 0.884207,0.144515 1.237091,0.416809 1.069791,-0.661429 2.113234,-0.533234 2.743231,-0.540442 -0.870758,0.18773 -1.443056,0.595056 -1.796318,0.951559 0.997868,0.232116 1.716321,0.780136 2.21246,1.083932 -1.053831,-0.416057 -1.880551,-0.362195 -2.388619,-0.245005 0.440574,0.429697 1.03941,1.6513038 1.469408,3.2506418 0.03075,-0.0012 0.06078,-0.0056 0.09154,-0.0056 0.877684,0 1.694975,0.442223 2.166119,1.172673 h -4.332254 c 0.30327,-0.469906 0.750376,-0.818437 1.264018,-1.008832 z'
icons['Desert']='m 56.685564,7.3015544 c -1.163468,0 -1.196356,1.1602241 -1.110911,2.3885401 0.0062,0.08855 0.0269,0.3224145 0.03835,0.4703645 -0.01349,-0.0057 -0.02624,-0.0097 -0.03997,-0.01565 -0.05116,-0.02386 -0.104422,-0.04911 -0.153917,-0.07885 -0.05005,-0.02916 -0.09867,-0.06095 -0.142037,-0.09558 -0.04518,-0.03348 -0.08195,-0.07157 -0.118272,-0.1085461 -0.03321,-0.03875 -0.06506,-0.07703 -0.08857,-0.117725 l -0.01836,-0.03024 -0.01457,-0.03078 c -0.01109,-0.01977 -0.01883,-0.04048 -0.02646,-0.06103 -0.0091,-0.0198 -0.01465,-0.0412 -0.02052,-0.06156 -0.007,-0.01983 -0.01048,-0.04077 -0.01405,-0.06103 -0.0044,-0.01996 -0.0082,-0.03927 -0.0092,-0.0594 -0.0014,-0.01988 -0.005,-0.0384 -0.0054,-0.05724 5.53e-4,-0.03819 -0.0035,-0.07128 5.53e-4,-0.103685 0.0025,-0.03177 0.0022,-0.05877 0.007,-0.08262 0.0065,-0.04657 0.01026,-0.0729 0.01026,-0.0729 l 0.0028,-0.01781 c 0.0028,-0.02168 0.0039,-0.04405 0.0032,-0.06643 -0.0082,-0.2837051 -0.301938,-0.5082681 -0.656176,-0.5016841 -0.354146,0.0065 -0.634626,0.241738 -0.626474,0.525446 0,0 0.0014,0.04498 0.0038,0.123126 0,0.03853 0.0065,0.08695 0.01241,0.142026 0.0044,0.05466 0.019,0.118633 0.0324,0.186849 0.0074,0.03427 0.01936,0.0705 0.02971,0.107468 0.0098,0.03697 0.02378,0.07525 0.03942,0.114485 0.01479,0.03927 0.03083,0.07935 0.05185,0.119885 0.01974,0.04066 0.03996,0.08217 0.06589,0.1231282 0.0243,0.04121 0.04985,0.08266 0.08047,0.1231263 0.0149,0.02021 0.0298,0.04055 0.04483,0.06102 0.0167,0.01969 0.03339,0.03953 0.05022,0.0594 0.06722,0.07945 0.146632,0.153272 0.229528,0.222492 0.08526,0.067 0.173589,0.130773 0.26679,0.183609 0.09208,0.05447 0.186834,0.100688 0.279753,0.140409 0.09278,0.04035 0.184979,0.07216 0.272194,0.100445 0.08763,0.02663 0.170279,0.0493 0.247887,0.06642 0.109884,0.02538 0.188687,0.03506 0.270031,0.04644 0.09771,1.201712 0.221968,2.725517 0.221968,2.725517 0.654474,0.262119 1.28049,0 1.28049,0 0,0 0.03238,-0.615052 0.104232,-1.41217 0.006,7.7e-5 0.0112,0.0011 0.01728,0.0011 0.172665,0.0014 0.359568,-0.02226 0.535203,-0.0729 0.176515,-0.04976 0.343434,-0.122522 0.494159,-0.207912 0.150997,-0.08517 0.287014,-0.184326 0.411527,-0.286213 0.0056,-0.0044 0.01584,-0.01322 0.02483,-0.02107 l 0.02702,-0.0243 c 0.01902,-0.01667 0.03363,-0.03027 0.04968,-0.04536 0.0316,-0.02977 0.0616,-0.06052 0.09073,-0.0918 0.0585,-0.06254 0.112795,-0.127675 0.16202,-0.194411 0.196974,-0.267987 0.313712,-0.557094 0.379123,-0.82408 0.06579,-0.267951 0.08354,-0.516617 0.08317,-0.7274169 -0.0028,-0.4220606 -0.07514,-0.6957266 -0.08047,-0.7171686 -0.08239,-0.275083 -0.427245,-0.444552 -0.770671,-0.378559 -0.343428,0.06599 -0.555491,0.342704 -0.473096,0.61779 -0.0071,-0.02223 0.07295,0.19027 0.106394,0.514108 0.0164,0.1612672 0.02024,0.3494995 -0.0075,0.5416465 -0.02724,0.19211 -0.08841,0.386635 -0.190102,0.555147 -0.02547,0.04206 -0.05341,0.08225 -0.08371,0.120966 -0.01501,0.01944 -0.03057,0.03839 -0.04698,0.0567 l -0.02323,0.02591 -0.01026,0.01081 c -0.0035,0.0038 -0.0055,0.0067 -0.01297,0.01349 -0.08183,0.07818 -0.163617,0.145563 -0.245731,0.199811 -0.08174,0.05425 -0.161785,0.0942 -0.234385,0.120427 -0.04339,0.01604 -0.08312,0.02555 -0.121516,0.0324 0.05536,-0.488415 0.120659,-0.986983 0.207383,-1.447271 0.29087,-1.5434645 0.274437,-2.7671026 -0.888942,-2.7671026 z'
icons['Marine']='m 58.445987,7.7889011 c -0.638143,0.03278 -1.251912,0.722382 -1.076886,2.1368891 -0.149665,-1.6992711 1.682136,-2.0138581 1.592648,-0.348319 -0.07182,1.3359668 -1.262364,1.9319968 -2.37736,1.6622048 0.139509,-0.26925 0.233684,-0.608768 0.260851,-1.029833 0.118995,-1.8442418 -2.01379,-1.8977109 -1.747644,0.252736 -0.11379,-1.2916838 1.278856,-1.5312358 1.21082,-0.265156 -0.07692,1.431197 -1.842811,1.745773 -2.757022,0.674494 0.309478,1.088012 1.860187,1.625113 2.721919,0.790601 1.248894,0.784773 3.231527,0.461637 3.394837,-2.0688448 0.07827,-1.2130891 -0.584019,-1.8375541 -1.222163,-1.8047721 z m -2.214798,4.5373109 c -0.762524,-0.04648 -1.248805,1.060036 -2.570703,0.08424 1.289347,1.741323 1.97123,-0.261029 3.13939,0.663155 1.16816,0.924181 2.048742,-0.901362 2.927683,-0.343458 -1.030863,-0.950927 -1.742241,0.776754 -2.910401,-0.14743 -0.219031,-0.173284 -0.410005,-0.245783 -0.585969,-0.256511 z'
icons['Grasslands']='m 56.396937,7.2847498 c -0.0023,1.929e-4 -0.08755,0.110394 -0.113689,0.2408121 -0.03427,0.131104 -0.0094,0.2822803 0.02119,0.3808553 0.08219,0.1953479 0.257702,0.3448124 0.253214,0.3451984 0.0045,-3.86e-4 0.267735,-0.1865408 0.185521,-0.3818887 -0.03054,-0.09857 -0.164828,-0.2409116 -0.240297,-0.3627686 -0.08357,-0.121212 -0.108286,-0.2220156 -0.105937,-0.2222085 z m 1.119312,0.3927406 c -0.0018,-0.0011 -0.17318,0.0287 -0.326078,0.1028362 -0.158983,0.07036 -0.299302,0.1851591 -0.380854,0.2676839 -0.147177,0.17492 -0.184153,0.3679722 -0.187587,0.3658693 0.0034,0.0021 0.37424,0.015263 0.521414,-0.1596803 0.08158,-0.0825 0.138502,-0.2491586 0.213942,-0.3710368 0.06933,-0.1256541 0.157356,-0.206788 0.159163,-0.2056723 z m -1.767851,0.1958539 c 0.0018,-0.00112 0.08983,0.079479 0.159163,0.2051555 0.07547,0.121856 0.132331,0.2885067 0.213942,0.3710366 0.147177,0.1749419 0.518015,0.1617802 0.521415,0.1596802 -0.0034,0.0021 -0.04038,-0.1909273 -0.187587,-0.3658692 C 56.373778,8.1608225 56.233459,8.0460037 56.074476,7.9756637 55.921608,7.9015065 55.750198,7.8722441 55.748398,7.8733443 Z m 1.767851,0.3255615 c -0.0018,-0.0011 -0.17318,0.028183 -0.326078,0.1023192 -0.158983,0.07036 -0.299302,0.1856758 -0.380854,0.2682007 -0.147177,0.174942 -0.184153,0.3674554 -0.187587,0.3653525 0.0034,0.0021 0.37424,0.015778 0.521414,-0.1591635 0.08158,-0.08253 0.138502,-0.2491587 0.213942,-0.3710369 0.06933,-0.1256538 0.157356,-0.206788 0.159163,-0.205672 z m -1.767851,0.1953369 c 0.0018,-0.00112 0.08983,0.079996 0.159163,0.2056723 0.07547,0.1218559 0.132331,0.2885066 0.213942,0.3710365 0.147177,0.174942 0.518015,0.1612636 0.521415,0.1591636 -0.0034,0.0021 -0.04038,-0.1904103 -0.187587,-0.3653523 C 56.373778,8.6822586 56.233459,8.5669021 56.074476,8.4965621 55.921608,8.4224049 55.750198,8.3931426 55.748398,8.3942427 Z m 1.767851,0.3255616 c -0.0018,-0.0011 -0.17318,0.028679 -0.326078,0.1028361 -0.158983,0.07034 -0.299302,0.1851589 -0.380854,0.2676837 -0.147177,0.174942 -0.184153,0.3679722 -0.187587,0.3658693 0.0034,0.0021 0.37424,0.015263 0.521414,-0.1596803 0.08158,-0.08253 0.138502,-0.2491586 0.213942,-0.3710368 0.06933,-0.1256538 0.157356,-0.2067671 0.159163,-0.205672 z m -1.767851,0.1958536 c 0.0018,-0.00112 0.08983,0.079501 0.159163,0.2051555 0.07547,0.1218779 0.132331,0.2885167 0.213942,0.3710366 0.147177,0.174943 0.518015,0.1617802 0.521415,0.1596802 -0.0034,0.0021 -0.04038,-0.1909492 -0.187587,-0.365869 C 56.373778,9.2031361 56.233459,9.0883173 56.074476,9.0179773 55.921608,8.9438212 55.750198,8.914558 55.748398,8.9156579 Z m 1.767851,0.3255615 c -0.0018,-0.0011 -0.17318,0.028163 -0.326078,0.1023194 -0.158983,0.07034 -0.299302,0.1856756 -0.380854,0.2682005 -0.147177,0.174942 -0.184153,0.3674762 -0.187587,0.3653522 0.0034,0.0021 0.37424,0.01526 0.521414,-0.1596799 0.08158,-0.08252 0.138502,-0.2491587 0.213942,-0.3710369 0.06933,-0.1256329 0.157356,-0.2062713 0.159163,-0.2051553 z m -2.822567,0.060978 c 0.0074,-0.0017 0.0057,0.2654744 0.139009,0.6650761 0.126037,0.4013595 0.387525,0.9356095 0.749308,1.5084355 0.361844,0.572804 0.673539,1.09461 0.850077,1.484147 0.04279,0.09023 0.07816,0.173267 0.107487,0.24753 l -0.01344,0.603581 0.212391,-0.135909 -0.01654,-0.460437 c 0.02981,-0.076 0.06592,-0.161748 0.110069,-0.254765 0.176512,-0.389537 0.488752,-0.911343 0.850596,-1.484147 0.361812,-0.572826 0.622845,-1.107076 0.748792,-1.5084355 0.133472,-0.3996017 0.131601,-0.6668142 0.139009,-0.6650761 -0.0074,-0.0017 -0.0558,0.2535055 -0.239778,0.6413051 -0.176512,0.3895585 -0.488265,0.9118595 -0.85008,1.4846645 -0.361873,0.572825 -0.623332,1.107074 -0.749308,1.508435 -0.0061,0.01807 -0.01205,0.03572 -0.01757,0.05323 l 0.03359,-1.882055 -0.09819,0.198437 -0.10387,1.667082 c -0.0039,-0.01212 -0.0077,-0.02431 -0.01188,-0.03669 C 56.407359,12.535241 56.145898,12.000992 55.784056,11.428167 55.422273,10.855362 55.110517,10.333061 54.933976,9.9435025 54.749998,9.5557032 54.701119,9.3004596 54.693682,9.3021974 Z m 1.054716,0.1343586 c 0.0018,-0.00112 0.08983,0.080018 0.159163,0.2056722 0.07547,0.121878 0.132331,0.2885366 0.213942,0.3710378 0.147177,0.174943 0.518015,0.161263 0.521415,0.159163 -0.0034,0.0021 -0.04038,-0.1904325 -0.187587,-0.3653529 C 56.373778,9.7245513 56.233459,9.6092157 56.074476,9.5388757 55.921608,9.4647196 55.750198,9.4354564 55.748398,9.436556 Z m 1.767851,0.3255616 c -0.0018,-0.0011 -0.17318,0.0287 -0.326078,0.1028361 -0.158983,0.07034 -0.299302,0.1851593 -0.380854,0.2676843 -0.147177,0.174942 -0.184153,0.367972 -0.187587,0.365869 0.0034,0.0021 0.37424,0.01528 0.521414,-0.15968 0.08158,-0.0825 0.138502,-0.249158 0.213942,-0.3710375 0.06933,-0.1256328 0.157356,-0.2067879 0.159163,-0.2056719 z m -1.767851,0.1958539 c 0.0018,-0.0011 0.08983,0.0795 0.159163,0.2051555 0.07547,0.121878 0.132331,0.288537 0.213942,0.371037 0.147177,0.174963 0.518015,0.16178 0.521415,0.15968 -0.0034,0.0021 -0.04038,-0.190949 -0.187587,-0.365869 -0.08155,-0.08253 -0.221872,-0.197344 -0.380855,-0.267684 -0.152868,-0.07414 -0.324278,-0.1034195 -0.326078,-0.1023195 z m 1.767851,0.3255625 c -0.0018,-0.0011 -0.17318,0.02818 -0.326078,0.102319 -0.158983,0.07036 -0.299302,0.185696 -0.380854,0.2682 -0.147177,0.174943 -0.184153,0.367477 -0.187587,0.365353 0.0034,0.0021 0.37424,0.01526 0.521414,-0.15968 0.08158,-0.08253 0.138502,-0.249159 0.213942,-0.371037 0.06933,-0.125633 0.157356,-0.206271 0.159163,-0.205155 z m -1.767851,0.195336 c 0.0018,-0.0011 0.08983,0.08002 0.159163,0.205673 0.07547,0.121878 0.132331,0.288506 0.213942,0.371036 0.147177,0.174941 0.518015,0.161264 0.521415,0.159164 -0.0034,0.0021 -0.04038,-0.190411 -0.187587,-0.365353 -0.08155,-0.08252 -0.221872,-0.19786 -0.380855,-0.2682 -0.152868,-0.07416 -0.324278,-0.103419 -0.326078,-0.10232 z'

#File Statics
season = '01'
fileSuffix = '.svg'
qrSuffix = '-QR.png'
qrPngScale = 6
qrModuleColor = [0, 0, 0, 255]  #Pure Black
qrBgColor = [0xff, 0xff, 0xff]  #Pure White
imgSuffix = '-img.'
creatureFile = '../assets/carddata/' + season + '/creature.csv'

#Constants from template
templateAnimalNameTxt = 'AnimalName'
templateAnimalGenusSpeciesTxt = 'AnimalGenusSpecies'
templateDescriptionTxt = 'Description text.  Description text.  Description text.  Description text.  ' \
                         'Description text.  Description text.  Description text.  Description text.  ' \
                         'Description text.  Description text.  Description text.  Description text.  ' \
                         'Description text.  Description text.  Description text.  Description text.  ' \
                         'Description text.  Description text.  Description text.  Description text.  ' \
                         'Description text.'
templateAttackTxt = 'ATT'
templateDefenseTxt = 'DEF'
templateAgilityTxt = 'AGI'
templateAnimalsInPlayTxt = 'AIP'
templateMassTxt = 'CreatureMass'
templateHeightTxt = 'CreatureHeight'
templateSpeedTxt = 'CreatureSpeed'
templateCreatureFoodChain = 'CreatureFoodChain'
templateCreatureStatus = 'CreatureStatus'
templateQRCode = r'file:///C:/project/cardgame/assets/qr/'
templateQRAbsref = "C:\\project\\cardgame\\assets\\qr\\"
templateBannerId = 'path60'
templateIconBackgroundId = 'path188-0'
templateIconId = 'path2-4'
templateAnimalImg = 'image1792'
templateImgHeight = 135
templateImgWidth = 230
templateImgHeightPx = 35.71875
templateImgWidthPx = 60.85417
templateHeaderId = 'layer7'
templateMaxSummon = 8
templateSummonImgBaseId = 'summon-'
templateSummonBGDefaultColor = 'ffff7c'
templateBackgroundId = 'background1'
templateAbilityLayerId = 'layer8'
templateAbilityId = 'ability'
templateBGPatternId = 'image3908'
templateDummyBgImg = 'rect5435'

#Read from creature csv
with open(creatureFile) as csvfile:
    reader = csv.DictReader(csvfile)
    for card in reader:
        #Params for card
        cardNo = ('000' + str(card['CardNum']))[-3:]
        biome = card['Biome']
        animalName = card['Name']
        animalGenusSpecies = card['GenusSpecies']
        animalDescription = card['Description']
        cardType = card['CardType']
        ability = card['Ability']
        attack = str(card['Attack'])
        defense = str(card['Defense'])
        agility = str(card['Agility'])
        animalsReq = str(card['AnimalsInPlay'])
        infoLink = card['InfoLink']
        mass = card['Mass']
        speed = card['Speed']
        height = card['Height']
        foodChain = card['FoodChain']
        status = card['Status']
        summon = card['Summon']
        animalImgLink = card['ImgLink']
        if card['KeepTop'] == 'TRUE':
            keepTop = True
        else:
            keepTop = False
        if card['KeepBottom'] == 'TRUE':
            keepBottom = True
        else:
            keepBottom = False
        if card['KeepLeft'] == 'TRUE':
            keepLeft = True
        else:
            keepLeft = False
        if card['KeepRight'] == 'TRUE':
            keepRight = True
        else:
            keepRight = False

        #Path issues
        imgType = animalImgLink[-3:]
        qrPngPath = '../assets/qr/' + season + '/' + cardNo + '-' + animalName.replace(" ", "") + qrSuffix
        templatePath = '../assets/templates/' + season + '/' + cardType + 'Template3.svg'
        CardOutputPath = '../assets/cards/' + season + '/' + cardNo + '-' + biome + '-' + cardType + '-' + animalName.replace(" ", "") + fileSuffix
        qrRef = season + '\\' + cardNo + '-' + animalName.replace(" ", "") + qrSuffix
        imgRef = season + '\\' + cardNo + '-' + animalName.replace(" ", "") + imgSuffix + imgType
        imgSavePath = '../assets/img/' + season + '/' + cardNo + '-' +animalName.replace(" ", "") + imgSuffix + imgType

        #Create QR Code
        url = pyqrcode.create(infoLink)
        url.png(qrPngPath, qrPngScale, qrModuleColor, qrBgColor)

        #Create IMG Asset
        if keepTop:               # Give TOP priority over BOTTOM
            keepBottom = False
        if keepLeft:              # Give LEFT priority OVER RIGHT
            keepRight = False

        #http = urllib3.PoolManager( )
        #r = http.request('GET', animalImgLink, preload_content=False)   # Download image
        #with open(imgSavePath, 'wb') as f:
        #    while True:
        #        data = r.read()
        #        if not data:
        #            break
        #        f.write(data)

        boxAspect = templateImgWidth / templateImgHeight
        img = Image.open(imgSavePath)
        imgAspect = img.width / img.height

        if imgAspect > boxAspect:   # Image has wider aspect ration than box
            newWidth = int(round((img.height / templateImgHeight) * templateImgWidth))
            deltaWidth = img.width - newWidth
            if keepLeft:
                leftTrim = 0
                rightTrim = newWidth
            elif keepRight:
                rightTrim = 0
                leftTrim = deltaWidth
            else:
                leftTrim = int(deltaWidth / 2)
                rightTrim = deltaWidth - leftTrim
            cropped = img.crop( (leftTrim, 0, newWidth, img.height) )
            cropped.save(imgSavePath)
            #TODO Test side trim

        elif imgAspect < boxAspect: # Image has a narrower aspect than box
            newHeight = int(round((img.width / templateImgWidth) * templateImgHeight))
            deltaHeight = img.height - newHeight
            if keepTop:
                topTrim = 0
                bottomTrim = newHeight
            elif keepBottom:
                bottomTrim = 0
                topTrim = deltaHeight
            else:
                topTrim = int(deltaHeight / 2)
                bottomTrim = deltaHeight - topTrim
            cropped = img.crop( (0, topTrim, img.width, newHeight) )
            cropped.save(imgSavePath)

        #create xml tree
        e = etree.parse(templatePath)

        # set the reference for the QR code to file generated above
        root = e.getroot()
        for img in root.findall('.//{http://www.w3.org/2000/svg}image'):
            if img.get('{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}absref') == templateQRAbsref:
                img.set('{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}absref', templateQRAbsref + qrRef)

        matchTxt = 'fill:#'
        matchLen = len(matchTxt)
        styleTxt = ''
        currentColor = ''
        setColor = bgColor[biome]
        colorLen = len(setColor)
        templateIMG = r'file:///C:/project/cardgame/assets/img/' + season + '/' + cardNo + '-' + animalName.replace(" ", "") + imgSuffix + imgType
        templateIMGAbsref = "C:\\project\\cardgame\\assets\img\\" + season + '\\' + cardNo + '-' + animalName.replace(" ", "") + imgSuffix + imgType
        summonList = []

        for i in range(0, templateMaxSummon):
            try:
                summonList.append(summon[i])
            except:
                # perhaps only check for the SPECIFIC error here
                summonList.append(None)

        for element in e.iter():
            if element.text == templateAnimalNameTxt:
                element.text = animalName.upper()
            elif element.text == templateAnimalGenusSpeciesTxt:
                element.text = animalGenusSpecies
            elif element.text == templateDescriptionTxt:
                element.text = animalDescription
            elif element.text == templateAttackTxt:
                element.text = attack
            elif element.text == templateDefenseTxt:
                element.text = defense
            elif element.text == templateAgilityTxt:
                element.text = agility
            elif element.text == templateAnimalsInPlayTxt:
                element.text = animalsReq
            elif element.text == templateMassTxt:
                element.text = mass
            elif element.text == templateHeightTxt:
                element.text = height
            elif element.text == templateSpeedTxt:
                element.text = speed
            elif element.text == templateCreatureFoodChain:
                element.text = foodChain
            elif element.text == templateCreatureStatus:
                element.text = status
            elif element.get('id') in (templateBannerId, templateIconBackgroundId):
                styleTxt = element.get('style')
                currentColor = styleTxt[styleTxt.find(matchTxt)+matchLen:styleTxt.find(matchTxt) + matchLen + colorLen]
                element.set('style', styleTxt.replace(currentColor, setColor))
            elif element.get('id') == templateIconId:
                element.set('d', icons[biome])
            elif element.get('id') == templateAnimalImg:
                element.set('{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}absref', templateIMGAbsref)
                element.set('{http://www.w3.org/1999/xlink}href', templateAnimalImg)
            elif element.get('id') == templateBGPatternId:
                myBgImg = 'bg_' + biome.lower() + '.png'
                bgImg = element.get('{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}absref')
                element.set('{http://www.w3.org/1999/xlink}href', '..\\..\\templates\\' + season + '\\' + myBgImg)
                element.set('{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}absref', bgImg + season + '\\' + myBgImg)
            elif element.get('id') == templateHeaderId:
                for child in element.getchildren():
                    i = int(child.get('id')[-1:])
                    if i < len(summon):
                        summonImage = summonImg[summon[i].lower()]
                        absRef = child.get('{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}absref')
                        href = child.get('{http://www.w3.org/1999/xlink}href')
                        child.set('{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}absref', absRef + season + '\\' + summonImage)
                        child.set('{http://www.w3.org/1999/xlink}href', href + season + '\\' + summonImage)
                    else:
                        element.remove(child)
            elif element.get('id') == templateAbilityId:
                abilityImgType = '.png'
                abilityImg = ability.lower().replace(" ", "_") + abilityImgType
                abilityAbsref = element.get('{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}absref')
                element.set('{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}absref', abilityAbsref + season + '\\' + abilityImg)
                element.set('{http://www.w3.org/1999/xlink}href', '..\\..\\templates\\' + season + '\\' + abilityImg)

        #write out the new SVG
        e.write(CardOutputPath)
